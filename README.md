# RK3399 utility : get CPU/GPU temperature

A simple console based application for getting temperature of CPU and GPU.

## How to make ?
* install g++ on your Rock960 board with is if you didn't have it.
```
sudo apt-get install g++
sudo apt-get make
```
* Then just type 'make' to build source code.

## Options
* --help | -h : shows help message.
* --noverbose | -nv : not shows message except CPU, GPU temperature.
* --repeat | -r : repeats print temperature in every 1 second.
* --iterline | -il : do not makes new line in repeats print.

