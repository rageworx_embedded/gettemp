# Makefile for rk3399 get cputemp.
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
GCC = gcc
GPP = g++
AR  = ar

# TARGET settings
TARGET_PKG = gettemp
TARGET_DIR = ./bin
TARGET_OBJ = ./obj/Release

# Compiler optiops 
COPTS  = -ffast-math -fexceptions -fopenmp -O3 -s

# CC FLAG
CFLAGS  = -I$(SRC_PATH) -I$(FSRC_PATH) 
CFLAGS += -I$(LIB_PATH) -I$(LOCI_PATH) -Ires
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# LINK FLAG
LFLAGS  = -static

# Sources
SRC_PATH = src
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)

.PHONY: prepare clean

all: prepare clean continue packaging

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_DIR)/res
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

packaging:
	@#cp -rf LICENSE ${TARGET_DIR}
	@#cp -rf readme.md ${TARGET_DIR}
	@#cp -rf res/*.png ${TARGET_DIR}/res

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Compiling $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
