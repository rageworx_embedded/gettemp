// Programmed by Raphael Kim @ MEDIT
// 

#include <unistd.h>
#include <signal.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#define DEF_VIRTUAL_TEMP_MAP "/sys/devices/virtual/thermal/thermal_zone%u/temp"
#define DEF_APP_VERSION      "1.2"

static bool opt_help = false;
static bool opt_verbose = true;
static bool opt_repeat = false;
static bool opt_iterline = false;
static unsigned opt_rpt_intsec = 1;

// detype 
//   0 : CPU
//   1 : GPU
float read_temperature( unsigned dtype )
{
    char dev_map[256] = {0};
    sprintf( dev_map, DEF_VIRTUAL_TEMP_MAP, dtype );

    FILE* fp = fopen( dev_map, "r" );

    if ( fp != NULL )
    {
        char data[32] = {0};

        fgets( data, 32, fp );

        fclose( fp );

        size_t datalen = strlen(data) - 1;

        if ( datalen > 4 )
        {
            char tmp_h[4] = {0};
            char tmp_l[4] = {0};

            size_t tmpsz_h = datalen - 3;
            strncpy( tmp_h, data, tmpsz_h );
            strncpy( tmp_l, &data[tmpsz_h], 3 );

            char conv_s[32] = {0};

            sprintf( conv_s, "%s.%s", tmp_h, tmp_l );

            return atof( conv_s );
        }
    }

    return -99.f;
}

void parseparam( int argc, char** argv )
{
    for( int cnt=1; cnt<argc; cnt++ )
    {
        if ( ( strcmp( argv[cnt], "--help" ) == 0 ) ||
             ( strcmp( argv[cnt], "-h" ) == 0 ) )
        {
            opt_help = true;
            return;
        }
        else
        if ( ( strcmp( argv[cnt], "--repeat" ) == 0 ) ||
             ( strcmp( argv[cnt], "-r" ) == 0 ) )
        {
            opt_repeat = true;
        }
        else
        if ( ( strcmp( argv[cnt], "--noverbose" ) == 0 ) ||
             ( strcmp( argv[cnt], "-nv" ) == 0 ) )
        {
            opt_verbose = false;
        }
        else
        if ( ( strcmp( argv[cnt], "--iterline" ) == 0 ) ||
             ( strcmp( argv[cnt], "-il" ) == 0 ) )
        {
            opt_iterline = true;
        }
    }
}

void sighandler( int sig )
{
    if ( opt_iterline == true )
    {
        printf( "\n" );
    }
    printf( "\nSIGNAL:%d\n",sig );
    exit( 0 );
}

int main( int argc, char** argv )
{
    parseparam( argc, argv );

    if( opt_verbose == true )
    {
        printf( "RK3399 CPU/GPU temperature checker v%s: Raph.K\n",
                DEF_APP_VERSION );

        if ( opt_repeat == true )
        {
            printf( " - repeat option eabled.\n" );
        }
        
        printf( "\n" );
    }

    if ( opt_help == true )
    {
        printf( "  usage:  rk3399cputemp (options)\n" );
        printf( "\n" );
        printf( "  _options_ \n" );
        printf( "\n" );
        printf( "    -h  | --help      : shows options to use ( this. )\n" );
        printf( "    -r  | --repeat    : repeating check in 1 sec.\n" );
        printf( "    -il | --iterline  : iterate on same line with repeat.\n");
        printf( "    -nv | --noverbose : shows only temperatures.\n" );
        printf( "\n" );
        return 0;
    }

    signal( SIGINT, sighandler );

    while( true )
    {   
        float tfCPU = read_temperature( 0 );
        float tfGPU = read_temperature( 1 );

        time_t rawt;
        struct tm* ti= NULL;

        time( &rawt );
        ti = localtime( &rawt );

        if ( opt_iterline == true )
        {
            printf( "\r " );
        }

        printf( "[%04d/%02d/%02d %02d:%02d:%02d] -- ",
                ti->tm_year + 1900,
                ti->tm_mon + 1,
                ti->tm_mday,
                ti->tm_hour,
                ti->tm_min,
                ti->tm_sec );

	if ( tfGPU >= 0.f )
	{
            printf( "CPU : GPU = %.2f 'C : %.2f 'C   ", tfCPU, tfGPU );
	}
	else
	{
            printf( "CPU = %.2f 'C, nonGPU   ", tfCPU );    
	}
        
        if ( opt_repeat == false )
        {
            printf( "\n" );
            break;
        }

        if ( opt_iterline == true )
        {
            fflush( stdout );
        }
        else
        {
            printf( "\n" );
        }

        sleep( opt_rpt_intsec );
    }

    return 0;
}
